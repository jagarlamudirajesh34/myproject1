package com.srishsoft.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.srishsoft.spring.model.Employee;
import com.srishsoft.spring.service.EmployeeService;
import com.srishsoft.spring.util.ServiceStatus;

@RestController
@RequestMapping("/main")
public class MainController {

	@Autowired
	EmployeeService employeeService;

	@RequestMapping(value = "/getAllEmployees", method = RequestMethod.GET, produces = { "application/json" })
	@ResponseBody
	public ServiceStatus getAllEmployees() {
		ServiceStatus serviceStatus = new ServiceStatus();
		try {
			List<Employee> list = employeeService.findAllEmployees();
			System.out.println(list);
			serviceStatus.setResult(list);
			serviceStatus.setStatus("success");
			serviceStatus.setMessage("retrived all users successfully");
		} catch (Exception e) {
			e.printStackTrace();
			serviceStatus.setResult("Test User");
			serviceStatus.setStatus("success");
			serviceStatus.setMessage("retrived all users successfully");
		}
		return serviceStatus;

	}

}
