FROM tomcat:8

MAINTAINER 10101

RUN rm -rf /usr/local/tomcat/webapps/

COPY target/myproject-0.0.1.war /usr/local/tomcat/webapps/myproject.war

EXPOSE 8080

USER root

WORKDIR /usr/local/tomcat/webapps 

CMD ["catalina.sh", "run"]
